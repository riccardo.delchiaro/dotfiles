# Install theme power-level-9k
git clone https://github.com/bhilburn/powerlevel9k.git ~/.oh-my-zsh/custom/themes/powerlevel9k
cd ~/.oh-my-zsh/custom/themes/powerlevel9k
git pull

# Setzsh as default shell
#chsh -s /usr/bin/zsh

mkdir ~/bin
ln -s ~/cfg/dotfiles/dotpull ~/bin/dotpull
ln -s ~/cfg/dotfiles/dotpush ~/bin/dotpush
ln -s ~/cfg/dotfiles/dotpulldream ~/bin/dotpulldream
ln -s ~/cfg/dotfiles/micchelp ~/bin/micchelp

